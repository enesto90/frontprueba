import { useState } from 'react';
import axios from 'axios';

export default function Register() {
    const apiUrl = "https://prueba-back-7fc8a370c332.herokuapp.com";
const [email, setEmail] = useState('');
const [password, setPassword] = useState('');
const [nombre, setNombre] = useState('');


const handleSubmit = async (e) => {
    e.preventDefault();
    // Datos para enviar al backend
    const userData = { email, password, nombre };

    try {
        const response=await axios.post(apiUrl+'/auth/registro', userData);


    if (response.ok) {
        const data = await response.json();
        console.log('Registro exitoso:', data);
        router.push(`/login`);    } else {
        console.error('Error en el registro');
    }
    } catch (error) {
    console.error('Error en la solicitud:', error);
    }
};

return (
    <form onSubmit={handleSubmit}>
    <div>
        <label htmlFor="email">Correo electrónico:</label>
        <input
        type="email"
        id="email"
        value={email}
        onChange={(e) => setEmail(e.target.value)}
        required
        />
    </div>
    <div>
        <label htmlFor="nombre">Nombre:</label>
        <input
        type="nombre"
        id="nombre"
        value={nombre}
        onChange={(e) => setNombre(e.target.value)}
        required
        />
    </div>
    <div>
        <label htmlFor="password">Contraseña:</label>
        <input
        type="password"
        id="password"
        value={password}
        onChange={(e) => setPassword(e.target.value)}
        required
        />
    </div>
    <button type="submit">Registrarse</button>
    </form>
);
}