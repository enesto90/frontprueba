import { useEffect } from 'react';
import { useRouter } from 'next/router';

export default function Home() {
const router = useRouter();

useEffect(() => {
    // Redirige al usuario a la página de login
    router.push('/login');
}, [router]);

  return null; // O un componente de carga mientras se redirige
}