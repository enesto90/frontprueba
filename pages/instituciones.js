import { useEffect, useState } from 'react';
import styles from '../styles/Instituciones.module.css'; // Ajusta la ruta según corresponda
import { useRouter } from 'next/router';

export default function Home() {
    const [instituciones, setInstituciones] = useState([]);
    const router = useRouter();
    const apiUrl = "https://prueba-back-7fc8a370c332.herokuapp.com";
    const verDetalles = (id, display_name) => {
        localStorage.setItem('detalle', JSON.stringify({ id, display_name }));
        router.push(`/institucion/detalle`);
    };

    useEffect(() => {
    const fetchInstituciones = async () => {
        const response = await fetch(apiUrl+'/api/instituciones');
        const data = await response.json();
        setInstituciones(data.instituciones);
    };

    fetchInstituciones();
    }, []);

    return (
        <div className={styles.container}>
        <h1 className={styles.title}>Instituciones Financieras</h1>
        <div className={styles.gridContainer}>
            {instituciones.map((institucion) => (
            <div key={institucion.id} className={styles.institucionCard}>
                <img src={institucion.logo} alt={institucion.display_name} className={styles.logo} />
                <h3>{institucion.display_name}</h3>
                {/* Asume que institucion.buttonColor es un color válido */}
                <button
                className={styles.button}
                style={{ backgroundColor: institucion.buttonColor }}
                onClick={() => verDetalles(institucion.id, institucion.name)}
                >
                Ver balance
                </button>
            </div>
            ))}
        </div>
        </div>
    );
    }