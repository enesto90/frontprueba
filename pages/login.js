// pages/login.js
import { useState } from 'react';
import { useRouter } from 'next/router';
import Link from "next/link"
import styles from '../styles/Login.module.css'; // Ajusta la ruta según corresponda

export default function Login() {
const [email, setEmail] = useState('');
const [password, setPassword] = useState('');
const router = useRouter();
const apiUrl = "https://prueba-back-7fc8a370c332.herokuapp.com";


const handleSubmit = async (event) => {
    event.preventDefault();
    try {
    const response = await fetch(apiUrl+'/auth/login', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ email, password }),
    });
    const data = await response.json();
    if(response.ok){
        router.push('/instituciones');

    }

    if (data.token) {
        // Guarda el token en localStorage o en una cookie
        localStorage.setItem('token', data.token);
        // Redirecciona al usuario a la página principal o donde desees
        router.push('/instituciones');
    } else {
        // Maneja errores, como mostrar un mensaje de login fallido
        console.error('Login fallido');
    }
    } catch (error) {
    console.error('Error durante el login:', error);
    }
};

return (
    <div className={styles.container}>
    <form onSubmit={handleSubmit} className={styles.form}>
        <h2 className={styles.title}>Login</h2>
        <input
        type="text"
        value={email}
        onChange={(e) => setEmail(e.target.value)}
        placeholder="Username"
        required
        className={styles.input}
        />
        <input
        type="password"
        value={password}
        onChange={(e) => setPassword(e.target.value)}
        placeholder="Password"
        required
        className={styles.input}
        />
        <button type="submit" className={styles.button}>Login</button>
        <div>
        {/* Usar el componente Link para la redirección */}
        <Link href="/registro">¿No tienes cuenta? Regístrate
        </Link>
    </div>
    </form>
    </div>
);
}