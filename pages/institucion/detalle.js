import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';

export default function InstitucionDetalle() {
const router = useRouter();
const [cargando, setCargando] = useState(true); // Estado para controlar la carga
const { id, display_name } = router.query;
const apiUrl = "https://prueba-back-7fc8a370c332.herokuapp.com";
const [detalle, setDetalle] = useState({
    expense: '',
    income: '',
    balance: 0,
    balance_details: [] // Asumiendo que la respuesta incluye un array de cuentas
});
var balanceTotal;
useEffect(() => {
    const fetchInstitucionData = async () => {
        setCargando(true);
        // Asegúrate de que el id está definido
        const detalleInstitucion = JSON.parse(localStorage.getItem('detalle'));
        const response = await fetch(apiUrl+`/api/balance?name=${detalleInstitucion.display_name}&id=${detalleInstitucion.id}`)
        .then(response => response.json())
        .then(data => {
            balanceTotal=data["balance"]
            setCargando(false);
            setDetalle(data);
          console.log(data); // Aquí manejas la respuesta del servidor
        })
        .catch(error => {
            console.error("Error en la solicitud:", error);
        });
        if (cargando) {
            return <p>Cargando detalles de la institución...</p>;
        }

    };

    fetchInstitucionData();
}, [id]);

return (
    <div>
    {detalle ? (
        <div>
        <h1>Detalle de la Institución</h1>
        <p>Balance: {detalle.balance}</p>
        <p>Entradas: {detalle.income}</p>
        <p>Salidas: {detalle.expense}</p>
        <h2>Cuentas</h2>
        <table>
            <thead>
            <tr>
                <th>Nombre Cuenta</th>
                <th>Cantidad</th>
                <th>Tipo</th>
            </tr>
            </thead>
            <tbody>
            {detalle.balance_details.map((balance_details) => (
                <tr key={balance_details.id}>
                <td>{balance_details.description}</td>
                <td>{balance_details.amount}</td>
                <td>{balance_details.type}</td>
                </tr>
            ))}
            </tbody>
        </table>
        </div>
    ) : (
        <p>Cargando detalles de la institución...</p> // Mensaje de carga
    )}
    </div>
);
}